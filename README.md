# go-script  -  run go on the run.

## Description
This script will let you run Golang one liner.

## Installation
Copy it into `/usr/local/bin/` and set it to executable.

    WARNING: Make sure you read the code.
    NEVER EVER run foreign scripts without knowing them!

## Usage
`go-script "<cmd>"`

or

`echo "cmd" | go-script [<parameter>]`

### Examples
* `go-script "import \"fmt\"\nfunc main() {\nfmt.Println(\"Hello Welt\")\n}"`
* `go-script <<< "import \"fmt\"\nfunc main() {\nfmt.Println(\"Hello Welt\")\n}"`
* `echo "import \"fmt\"\nfunc main() {\nfmt.Println(\"Hello Welt\")\n}" | go-script`


## Authors and acknowledgment
Juergen 'sysdef' Heine <<go-script@sysdef.fr>>

## License
MIT License

## Project status
Just the first shoot. Feel free to send coffee and PRs.

## FAQ

### What the heck? Why would someone do this?
You don't have to. Just move on.
